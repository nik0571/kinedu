import angular from 'angular';
import ngRedux from 'ng-redux';
import { combineReducers } from 'redux';

import reducers from './redux/reducers/';

import './components/main/main.module';
import './components/header/header.module';
import './services/main.services';

require('./assets/styles/index.scss');

angular
.module('app', [
  ngRedux,
  'mainModule',
  'headerModule',
  'mainServices'
])
.config(($ngReduxProvider) => {
  let reducer = combineReducers(reducers);
  $ngReduxProvider.createStoreWith(reducer);
})
.controller('appController',  ['$scope', function ($scope) {
    $scope.current_view ='Hello Angular';
  }]
)
