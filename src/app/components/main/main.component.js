'use strict';

import {
  SET_AREA_PHYSICAL,
  SET_AREA_SOCIAL,
  SET_ACTIVE_AREA
} from '../../redux/constants';
import  actions from '../../redux/actions/main.actions';

export default function mainComponent () {
  return {
    controller: mainComponentCtrl,
    template: require('./main.component.html')
  };
}

function mainComponentCtrl ($timeout, $ngRedux, $scope, _Milestones) {
  const ctrl = this;
  ctrl.$onInit = init;

  this.get_milestones = (action) => {
    _Milestones
    .get(action)
  }

  this.mapStateToThis = (state) => {
    const { areas, activeArea } = state.mainReducer;
    return {
      activeArea,
      areas
    };
  }

  this.get_status_name = (status) => {
    let name = 'Not answered';
    if(status === true)
      name = 'Completed';
    if(status === false)
      name = 'Uncompleted';
    return name;
  }

  this.changeStatusActivity = (milestone) => {
    let status = 'Not answered';
    if(milestone.status === undefined)
      return milestone.status = true;
    return milestone.status = !milestone.status;
  }

  this.next = ( activeArea ) => {
    $ngRedux.dispatch({ type: SET_ACTIVE_AREA, activeArea });
  };

  function init (params) {
    $ngRedux.connect(this.mapStateToThis, actions)($scope);
    ctrl.get_milestones(SET_AREA_PHYSICAL);
    ctrl.get_milestones(SET_AREA_SOCIAL);
  }
}