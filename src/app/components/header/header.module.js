import headerComponent from './header.component';
require('./header.component.scss');

export default angular
  .module('headerModule', [])
  .component('headerComponent', headerComponent()).name