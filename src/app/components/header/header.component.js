'use strict';

import { SET_ACTIVE_AREA } from '../../redux/constants';
import  actions from '../../redux/actions/main.actions';

export default function headerComponent () {
  return {
    controller: headerComponentCtrl,
    template: require('./header.component.html')
  };
}

function headerComponentCtrl ($timeout, $ngRedux, $scope) {
  const ctrl = this;
  ctrl.$onInit = init;

  this.mapStateToThis = (state) => {
    const { areas, activeArea } = state.mainReducer;
    return {
      activeArea,
      areas
    };
  }

  this.changeArea = (activeArea) => {
    $ngRedux.dispatch({ type: SET_ACTIVE_AREA, activeArea });
  }

  function init (params) {
    ctrl.title = "Header Working!";
    $ngRedux.connect(this.mapStateToThis, actions)($scope);
  }
}