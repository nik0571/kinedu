import { SET_AREA_PHYSICAL, SET_AREA_SOCIAL, SET_ACTIVE_AREA } from '../constants';

const initialState = {
  activeArea: 'physical',
  areas: {
    physical: {},
    social: {}
  }
}

export default function mainReducer(state = initialState, action) {
  switch (action.type) {
    case SET_AREA_PHYSICAL:
      state.areas.physical = action.skill;
      return state;
    case SET_AREA_SOCIAL:
        state.areas.social = action.skill;
        return state
    case SET_AREA_SOCIAL:
        state.areas.social = action.skill;
        return state
        case SET_ACTIVE_AREA:
          state.activeArea = action.activeArea;
        return state
    default:
    return state;
  }
}
