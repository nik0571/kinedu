import {
  SET_AREA_PHYSICAL,
  SET_AREA_SOCIAL,
  SET_ACTIVE_AREA,
} from '../constants';

const setDefault = () => {
  return {
    type: DEFAULT
  };
}

const setAreaPhysical = () => {
  return {
    type: SET_AREA_PHYSICAL
  };
}

const setAreaSocial = () => {
  return {
    type: SET_AREA_SOCIAL
  };
}

const setActiveArea = () => {
  return {
    type: SET_ACTIVE_AREA
  };
}

export default { setAreaPhysical, setAreaSocial, setActiveArea };
