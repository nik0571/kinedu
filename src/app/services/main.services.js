import '../fatories/main.factory';
import { SET_AREA_PHYSICAL, SET_AREA_SOCIAL } from '../redux/constants';

export default angular
  .module('mainServices', ['mainFactories'])
  .service('_Milestones', function ($http, FCServer, $ngRedux) {

    const { base, Authorization } = FCServer;

    const type_milestone = (action) => {
      switch (action) {
        case SET_AREA_PHYSICAL:
          return 23;
        case SET_AREA_SOCIAL:
            return 2
        default:
        return undefined;
      }
    }

    const get = (action) => {
      return $http({
        method: 'GET',
        url: `${base}/${type_milestone(action)}/milestones`,
        headers: {
          'Authorization': Authorization
        },
      }).then(({ data: response }) => {
        const { skill } = response.data;
        $ngRedux.dispatch({ type: action, skill });
      }, (error) => {
        console.log("errorCallback", error);
      });;
    };

    return {
      get
    };
  });

